Rails.application.routes.draw do
  root 'main#index'
  resource :game_players, only: :create
  resources :games, only: [] do
    put 'start', action: 'start'
    put 'end', action: 'end'
    get 'hand', action: 'get_hand'

    resources :moves, only: :create, controller: :game_moves
  end
end
