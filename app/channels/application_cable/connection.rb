module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :current_player_id

    def connect
      self.current_player_id = cookies.signed[:current_player_id] || 'spectator'
    end
  end
end
