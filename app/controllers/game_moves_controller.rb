class GameMovesController < ApplicationController
  include GameConcern

  before_action :find_game, :check_authorization_for_current_player, :check_whose_move

  def create
    # validate out the wazoo
    ActiveRecord::Base.transaction do
      move = @game.moves.create(player: @current_player, score: score)
      tiles = @game.tiles.where(id: tile_ids)
      tiles.update_all(in_hand_of_player_id: nil, played_in_move_id: move.id)
    end

    @current_player.draw_hand!

    @game.broadcast!

    render status: :created, json: { hand: @current_player.hand.map(&:serialize) }
  end

  private

  def check_whose_move
    unless @game.current_player_to_move == @current_player
      return render status: :forbidden, json: { errors: ['It\'s not your turn'] }
    end
  end

  def tile_ids
    params[:tile_ids]
  end

  def score
    params[:score]
  end
end
