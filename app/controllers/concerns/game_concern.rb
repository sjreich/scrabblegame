module GameConcern
  def find_game
    @game = Game.find(params[:game_id])
    unless @game.present?
      return render status: :not_found, json: { errors: ['No such game'] }
    end
  end

  def check_authorization_for_current_player
    @current_player = GamePlayer.find_by(id: cookies.signed[:current_player_id])
    unless @game.players.include?(@current_player)
      return render status: :forbidden, json: { errors: ['Only players can affect the game'] }
    end
  end
end
