class MainController < ApplicationController
  def index
    @game = Game.not_ended.last || Game.create

    current_player = real_player || null_player

    @initial_game_state = @game.serialize
    @initial_hand = { hand: current_player.hand.map(&:serialize) }
  end

  private

  def real_player
    @game.players.find_by(id: cookies.signed[:current_player_id])
  end

  def null_player
    cookies.delete(:current_player_id)
    GamePlayer.new
  end
end
