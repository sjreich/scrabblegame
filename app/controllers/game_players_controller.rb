class GamePlayersController < ApplicationController
  def create
    player = GamePlayer.new(create_params)

    if player.save
      player.game.broadcast!
      cookies.signed[:current_player_id] = player.id
      render status: :created, json: { newPlayer: { id: player.id, name: player.name } }
    else
      render status: :bad_request, json: { errors: player.errors.full_messages }
    end
  end

  private

  def create_params
    params.require(:player).permit(:name, :game_id)
  end
end
