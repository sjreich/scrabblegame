class GamesController < ApplicationController
  include GameConcern

  before_action :find_game, :check_authorization_for_current_player

  def start
    if @game.start
      render status: :ok, json: { hand: @current_player.hand.map(&:serialize) }
    else
      render status: :bad_request, json: { errors: @game.errors.full_messages }
    end
  end

  def end
    if @game.end
      cookies.delete(:current_player_id)
      render status: :no_content, json: {}
    else
      render status: :bad_request, json: { errors: @game.errors.full_messages }
    end
  end

  def get_hand
    render status: :ok, json: { hand: @current_player.hand.map(&:serialize) }
  end
end
