class GameMove < ApplicationRecord
  belongs_to :game
  belongs_to :player, class_name: 'GamePlayer', foreign_key: 'game_player_id'
  has_many :tiles_played, class_name: 'GameTile', foreign_key: 'played_in_move_id', inverse_of: :played_in_move, dependent: :nullify

  default_scope { order(move_number: :asc) }

  around_create :assign_move_number!

  validates :score, numericality: { only_integer: true, greater_than_or_equal_to: 0 }

  def serialize
    tiles_played.map(&:serialize)
  end

  def assign_move_number!
    transaction do
      existing_moves = game.moves
      new_move_number = existing_moves.present? ? existing_moves.maximum(:move_number) + 1 : 1
      self.move_number = new_move_number
      yield
    end
  end
end
