module GameValidations
  def GameValidations.included(model_class)
    model_class.validate :cannot_unstart_game
    model_class.validate :cannot_end_unstarted_game
    model_class.validate :only_one_game_in_progress
    model_class.validate :move_numbers
  end

  def cannot_unstart_game
    return false unless has_started == false
    return false unless has_started_was == true

    errors.add(:has_started, "must be true. You cannot unstart a game once it has been started.")
  end

  def cannot_end_unstarted_game
    return false unless has_started == false
    return false unless has_ended == true

    errors.add(:has_ended, "must be false. You cannot end a game without starting it.")
  end

  def only_one_game_in_progress
    return false unless has_started == true
    return false unless has_ended == false
    return false unless has_started_was == false || has_ended_was == true
    return false if Game.in_progress.none?

    errors.add(:has_started, "must be false. You cannot have more than one game in progress at once.")
  end

  def move_numbers
    return false unless moves.map(&:move_number).sort == (1..moves.count)

    errors.add(:moves, "must be numbered from 1 to #{moves.count}")
  end
end
