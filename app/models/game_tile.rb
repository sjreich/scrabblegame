class GameTile < ApplicationRecord
  belongs_to :game
  belongs_to :in_hand_of_player, class_name: 'GamePlayer', required: false
  belongs_to :played_in_move, class_name: 'GameMove', required: false

  scope :undrawn, -> { where(in_hand_of_player: nil, played_in_move: nil) }

  def self.set_up_tiles_for(game)
    STANDARD_SET.each do |tile_type|
      tile_type[:count].times do
        create(
          game: game,
          letter: tile_type[:letter],
          points: tile_type[:points]
        )
      end
    end
  end

  def serialize
    {
      id: id,
      letter: letter,
      points: points,
    }
  end

  private

  STANDARD_SET = [
    { letter: 'A', points: 1, count: 9 },
    { letter: 'B', points: 3, count: 2 },
    { letter: 'C', points: 3, count: 2 },
    { letter: 'D', points: 2, count: 4 },
    { letter: 'E', points: 1, count: 12 },
    { letter: 'F', points: 4, count: 2 },
    { letter: 'G', points: 2, count: 3 },
    { letter: 'H', points: 4, count: 2 },
    { letter: 'I', points: 1, count: 9 },
    { letter: 'J', points: 8, count: 1 },
    { letter: 'K', points: 5, count: 1 },
    { letter: 'L', points: 1, count: 4 },
    { letter: 'M', points: 3, count: 2 },
    { letter: 'N', points: 1, count: 6 },
    { letter: 'O', points: 1, count: 8 },
    { letter: 'P', points: 3, count: 2 },
    { letter: 'Q', points: 10, count: 1 },
    { letter: 'R', points: 1, count: 6 },
    { letter: 'S', points: 1, count: 4 },
    { letter: 'T', points: 1, count: 6 },
    { letter: 'U', points: 1, count: 4 },
    { letter: 'V', points: 4, count: 2 },
    { letter: 'W', points: 4, count: 2 },
    { letter: 'X', points: 8, count: 1 },
    { letter: 'Y', points: 4, count: 2 },
    { letter: 'Z', points: 10, count: 1 },
    { letter: ' ', points: 0, count: 2 },
  ]
end
