class Game < ApplicationRecord
  include GameValidations

  has_many :moves, class_name: 'GameMove', dependent: :destroy
  has_many :players, class_name: 'GamePlayer', dependent: :destroy
  has_many :tiles, class_name: 'GameTile', dependent: :destroy

  scope :not_ended, -> { where(has_ended: false) }
  scope :started, -> { where(has_started: true) }
  scope :in_progress, -> { started.not_ended }

  def broadcast!
    reload

    GameChannel.broadcast_to(self, serialize)
  end

  def start
    transaction do
      update(has_started: true)
      set_up_tiles!
      players.each(&:draw_hand!)
    end

    broadcast!
    true
  end

  def end
    update(has_ended: true)

    broadcast!
    true
  end

  def undrawn_tiles
    tiles.undrawn
  end

  def serialize
    {
      gameState: {
        gameId: id,
        hasStarted: has_started,
        hasEnded: has_ended,
        players: players.map(&:serialize),
        undrawnTilesRemaining: undrawn_tiles_remaining,
        currentPlayerToMoveId: current_player_to_move&.id,
        moves: moves.map(&:serialize),
      }
    }
  end

  def current_player_to_move
    return nil unless has_started
    return nil if has_ended
    return nil if players.empty?
    return players.find { |player| player.order == 1 } if moves.empty?
    last_player = moves.last.player
    current_player_order = current_player_order(last_player.order)
    players.find { |player| player.order == current_player_order }
  end

  private

  def undrawn_tiles_remaining
    return 100 unless tiles.present?

    tiles.undrawn.count
  end

  def current_player_order(last_player_order)
    if last_player_order >= players.count
      1
    else
      last_player_order + 1
    end
  end

  def set_up_tiles!
    GameTile.set_up_tiles_for(self)
  end
end
