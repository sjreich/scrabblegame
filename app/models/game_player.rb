class GamePlayer < ApplicationRecord
  belongs_to :game
  has_many :tiles_in_hand, class_name: 'GameTile', foreign_key: 'in_hand_of_player_id', inverse_of: :in_hand_of_player, dependent: :nullify
  has_many :moves, class_name: 'GameMove', inverse_of: :player, dependent: :restrict_with_exception

  alias_method :hand, :tiles_in_hand

  validates :name, presence: true, uniqueness: { scope: :game }, length: { maximum: 50 }
  
  around_create :assign_order!

  def draw_hand!
    transaction do
      number_of_tiles_left = game.undrawn_tiles.count
      number_for_full_hand = 7 - tiles_in_hand.count
      number_to_draw = [number_for_full_hand, number_of_tiles_left].min

      number_to_draw.times { draw_tile! }
    end

    hand.reload
  end

  def assign_order!
    transaction do
      existing_players = game.players
      new_order = existing_players.present? ? existing_players.maximum(:order) + 1 : 1
      self.order = new_order
      yield
    end
  end

  def serialize
    {
      id: id,
      name: name,
      score: score,
    }
  end

  def score
    moves.sum(:score)
  end

  private

  def draw_tile!
    transaction do
      tile = game.undrawn_tiles.sample
      tile.update(in_hand_of_player: self)
    end
  end
end
