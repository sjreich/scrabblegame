import React from 'react';
import ReactDOM from 'react-dom';

class Tile extends React.Component {
  constructor(props) {
    super(props);
    this.toggleSelected = this.toggleSelected.bind(this);
  }

  toggleSelected(event) {
    if (!this.props.isMyTurn) { return; }
    this.props.toggleTileSelection(this.props.tile.id);
  }

  render() {
    const tile = this.props.tile;
    const cardClass = tile.selected ? "card tile grey darken-3" : "card tile";
    const contentClass = tile.selected ? "card-content white-text" : "card-content";

    return (
      <li onClick={this.toggleSelected}>
        <div className={cardClass}>
          <div className={contentClass}>
            <h4>
              {tile.letter}
              <small><sub>{tile.points}</sub></small>
            </h4>
          </div>
        </div>
      </li>
    );
  }
}

export default Tile;
