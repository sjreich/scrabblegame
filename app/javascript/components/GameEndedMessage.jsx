import React from 'react';
import ReactDOM from 'react-dom';

class GameEndedMessage extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return(
        <div className='card'>
          <div className='card-content'>
            The game is over.  Refresh the page to start a new game.
          </div>
        </div>
    );
  }
}

export default GameEndedMessage;
