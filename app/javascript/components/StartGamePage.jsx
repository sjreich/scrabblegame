import React from 'react';
import ReactDOM from 'react-dom';

class StartGamePage extends React.Component {
  constructor(props) {
    super(props);

    this.startGame = this.startGame.bind(this);
  }

  startGame() {
    const csrfToken = document.querySelector("[name='csrf-token']").content
    const url = `/games/${this.props.gameId}/start`;

    fetch(url, {
      method: 'put',
      headers: {
        'Content-type': 'application/json',
        "X-CSRF-Token": csrfToken
      },
      body: JSON.stringify({})
    })
    .then(
      (response) => {
        if (response.ok) {
          response.json().then((response) => {
            this.props.handleGameStart(response.hand);
          })
        } else {
          response.json().then((response) => {
            response.errors.forEach((error) => {
              M.toast({html: error});
            })
          }).catch((error) => {
            M.toast({html: 'Server error'});
          })
        }
      }
    )
  }

  render() {
    return (
      <div className='card'>
        <div className='card-content'>
          <p className='card-title'>Is everyone here?  When they are...</p>
          <div className='center-align'>
            <button onClick={this.startGame} className="btn waves-effect">
              Start Game!
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default StartGamePage;
