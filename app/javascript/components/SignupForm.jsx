import React from 'react';
import ReactDOM from 'react-dom';

class SignupForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { newName: '' };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();

    const csrfToken = document.querySelector("[name='csrf-token']").content
    fetch('/game_players', {
      method: 'post',
      headers: {
        'Content-type': 'application/json',
        "X-CSRF-Token": csrfToken
      },
      body: JSON.stringify({
        player: {
          name: this.state.newName,
          game_id: this.props.gameId
        }
      })
    })
    .then((response) => {
      if (response.ok) {
        response.json().then((response) => {
          this.props.addPlayer(response.newPlayer);
        })
      } else {
        response.json().then((response) => {
          response.errors.forEach((error) => {
            M.toast({html: error});
          })
        }).catch((error) => {
          M.toast({html: 'Server error'});
        })
      }
    })
  }

  handleChange(event) {
    this.setState({newName: event.target.value});
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <div className='card-panel'>
          <div className='input-field'>
            <input type="text" name="name" id='name' value={this.state.newName} onChange={this.handleChange} />
            <label htmlFor="name">Name</label>
          </div>

          <div className="right-align">
            <input className="btn waves-effect" type="submit" value="Enter" />
          </div>
        </div>
      </form>
    );
  }
}

export default SignupForm;
