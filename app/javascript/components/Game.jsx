import React from 'react';
import ReactDOM from 'react-dom';
import Tile from './Tile';

class Game extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};

    this.toggleTileSelection = this.toggleTileSelection.bind(this);
    this.submitMove = this.submitMove.bind(this);
    this.handleScoreChange = this.handleScoreChange.bind(this);
  }

  toggleTileSelection(tileId) {
    const newHand = this.props.hand.map((tile) => {
      if (tile.id === tileId) {
        tile.selected = !tile.selected;
        return tile;
      } else {
        return tile;
      }
    });

    this.props.setHand(newHand);
  }

  submitMove() {
    const score = this.state.scoreToRecord;
    const tile_ids = this.props.hand
                         .filter((tile) => { return tile.selected; })
                         .map((tile) => { return tile.id; })

    const csrfToken = document.querySelector("[name='csrf-token']").content
    const url = `/games/${this.props.gameState.gameId}/moves`;

    fetch(url, {
      method: 'post',
      headers: {
        'Content-type': 'application/json',
        "X-CSRF-Token": csrfToken
      },
      body: JSON.stringify({tile_ids: tile_ids, score: score})
    })
    .then(
      (response) => {
        if (response.ok) {
          response.json().then((response) => {
            this.props.setHand(response.hand);
          })
        } else {
          response.json().then((response) => {
            response.errors.forEach((error) => {
              M.toast({html: error});
            })
          }).catch((error) => {
            M.toast({html: 'Server error'});
          })
        }
      }
    )
  }

  playButton() {
    if (!this.props.isMyTurn) { return; }

    return(
      <button onClick={this.submitMove} className="btn waves-effect">
        Play!
      </button>
    )
  }

  placeToRecordScore() {
    if (!this.props.isMyTurn) { return; }

    return(
      <form>
        <div className='card-panel'>
          <div className='input-field'>
            <input type="text" name="score" id='score' value={this.state.scoreToRecord} onChange={this.handleScoreChange} />
            <label htmlFor="score">Score</label>
          </div>
        </div>
      </form>
    );
  }

  handleScoreChange(event) {
    this.setState({scoreToRecord: event.target.value});
  }

  render() {
    return (
      <React.Fragment>
        { this.playButton() }
        { this.placeToRecordScore() }

        <ul className="tiles">
          {this.props.hand.map((tile) => {
            return(<Tile key={tile.id}
                         tile={tile}
                         isMyTurn={this.props.isMyTurn}
                         toggleTileSelection={this.toggleTileSelection} />);
          })}
        </ul>

      </React.Fragment>
    );
  }
}

export default Game;
