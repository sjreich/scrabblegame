import React from 'react';
import ReactDOM from 'react-dom';

class Scoreboard extends React.Component {
  constructor(props) {
    super(props);
  }

  gameState() {
    return this.props.gameState;
  }

  playersSection() {
    const players = this.gameState().players;
    const currentPlayerToMove = players.find(player => player.id === this.gameState().currentPlayerToMoveId)

    if (players.length === 0) { return; }

    return (
      <table>
        <thead>
          <tr>
            <th>Name</th>
            <th>Score</th>
          </tr>
        </thead>

        <tbody>
          {players.map((player) => {
            const className = player === currentPlayerToMove ? 'active' : '';

            return(
              <tr className={className} key={player.id}>
                <td>{player.name}</td>
                <td>{player.score}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }

  gameStatusSection() {
    if (!this.gameState().hasStarted) {
      return 'Waiting for everyone to join...';
    } else if(!this.gameState().hasEnded) {
      return 'Game in progress...';
    } else {
      return 'Game complete.';
    }
  }

  undrawnTilesRemaining() {
    if (this.gameState().hasStarted) {
      return('Tiles left to be drawn: ' + this.gameState().undrawnTilesRemaining);
    }
  }

  render() {
    return (
      <div className='scoreboard card'>
        <div className="card-content">
          {this.gameStatusSection()}
          {this.playersSection()}
          {this.undrawnTilesRemaining()}
        </div>
      </div>
    );
  }
}

export default Scoreboard;
