import React from 'react';
import ReactDOM from 'react-dom';

class EndGameButton extends React.Component {
  constructor(props) {
    super(props);

    this.endGame = this.endGame.bind(this);
  }

  endGame() {
    const csrfToken = document.querySelector("[name='csrf-token']").content
    const url = `/games/${this.props.gameId}/end`;

    fetch(url, {
      method: 'put',
      headers: {
        'Content-type': 'application/json',
        "X-CSRF-Token": csrfToken
      },
      body: JSON.stringify({})
    })
    .then(
      (response) => {
        if (response.ok) {
          this.props.markGameEnd();
        } else {
          response.json().then((response) => {
            response.errors.forEach((error) => {
              M.toast({html: error});
            })
          }).catch((error) => {
            M.toast({html: 'Server error'});
          })
        }
      }
    )
  }

  render() {
    return(
      <div className='end-game-button'>
        <button onClick={this.endGame} className="btn waves-effect">
          End Game!
        </button>
      </div>
    );
  }
}

export default EndGameButton;
