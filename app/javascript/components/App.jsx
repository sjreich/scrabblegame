import React from 'react';
import ReactDOM from 'react-dom';
import docCookies from 'doc-cookies';
import { createConsumer } from '@rails/actioncable';
import EndGameButton from './EndGameButton';
import Game from './Game';
import GameEndedMessage from './GameEndedMessage';
import Scoreboard from './Scoreboard';
import SignupForm from './SignupForm';
import StartGamePage from './StartGamePage';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      gameState: props.initialGameState,
      hand: props.initialHand,
      actionCable: createConsumer()
    };

    this.state.currentPlayer = this.currentPlayer();

    this.addPlayer = this.addPlayer.bind(this);
    this.handleGameStart = this.handleGameStart.bind(this);
    this.markGameEnd = this.markGameEnd.bind(this);
    this.setHand = this.setHand.bind(this);
  }

  componentDidMount() {
    this.createActionCableConnection();
  }

  createActionCableConnection() {
    this.state.actionCable.subscriptions.create(
      { channel: 'GameChannel', game_id: this.gameState().gameId },
      {
        received: data => {
          if (!this.gameState().hasStarted && data.gameState.hasStarted) {
            this.getHand();
          }
          this.setState({gameState: data.gameState});
        }
      }
    );
  }

  resetActionCableConnection() {
    this.state.actionCable.disconnect();

    this.createActionCableConnection();
  }

  currentPlayer() {
    const signed_cookie = docCookies.getItem('current_player_id');
    if (!signed_cookie) return null;

    const id_in_base64 = signed_cookie.split('--')[0];
    const id = parseInt(atob(id_in_base64));
    const player = this.gameState().players.find(player => {
      return player['id'] === id
    });

    return player;
  }

  gameState() {
    return this.state.gameState;
  }

  gameId() {
    return this.gameState().gameId;
  }

  setHand(hand) {
    this.setState({hand: hand});
  }

  getHand() {
    const csrfToken = document.querySelector("[name='csrf-token']").content
    const url = `/games/${this.gameId()}/hand`;

    fetch(url, {
      method: 'get',
      headers: {
        'Content-type': 'application/json',
        "X-CSRF-Token": csrfToken
      }
    })
    .then(
      (response) => {
        if (response.ok) {
          response.json().then((response) => {
            this.setState({hand: response.hand});
          });
        } else {
          response.json().then((response) => {
            response.errors.forEach((error) => {
              M.toast({html: error});
            })
          }).catch((error) => {
            M.toast({html: 'Server error'});
          })
        }
      }
    )
  }

  addPlayer(newPlayer) {
    this.setState((state) => {
      const players = state.gameState.players;
      const player_already_in_list = players.find((player) => {
        return player.name === newPlayer.name;
      })

      if (player_already_in_list) {
        return({
          ...state,
          currentPlayer: newPlayer
        });
      } else {
        return({
          ...state,
          currentPlayer: newPlayer,
          gameState: {
            ...state.gamestate,
            players: players.concat(newPlayer)
          }
        });
      }
    });
  }

  isMyTurn() {
    if (this.state.currentPlayer) {
      return(this.state.gameState.currentPlayerToMoveId === this.state.currentPlayer.id)
    } else {
      return false;
    }
  }

  handleGameStart(hand) {
    this.setState((state) => {
      return ({
        ...state,
        hand: hand,
        gameState: {
          ...state.gameState,
          hasStarted: true
        }
      });
    });
  }

  markGameEnd() {
    this.setState((state) => {
      return({
        ...state,
        currentPlayer: null,
        gameState: {
          ...state.gameState,
          hasEnded: true
        }
      });
    });
  }

  centerpiece() {
    if (this.gameState().hasStarted) {
      if (this.gameState().hasEnded) {
        return(<GameEndedMessage />);
      } else {
        return(this.game());
      }
    } else if (this.state.currentPlayer) {
      return (this.startGamePage());
    } else {
      return (this.signupForm());
    }
  }

  game() {
    return(
      <Game
        gameState={this.state.gameState}
        hand={this.state.hand}
        isMyTurn={this.isMyTurn()}
        setHand={this.setHand}
      />
    );
  }

  scoreboard() {
    return(
      <Scoreboard gameState={this.state.gameState}/>
    );
  }

  startGamePage() {
    return(
      <StartGamePage
        gameId={this.gameId()}
        handleGameStart={this.handleGameStart}
      />
    );
  }

  signupForm() {
    return(
      <SignupForm
        gameId={this.gameId()}
        addPlayer={this.addPlayer}
      />
    );
  }

  endGameButton() {
    const gameState = this.gameState();
    const displayable = gameState.hasStarted &&
                          !gameState.hasEnded &&
                          this.state.currentPlayer;

    if (displayable) {
      return(
        <EndGameButton
          gameId={gameState.gameId}
          markGameEnd={this.markGameEnd}
        />
      );
    }
  }

  render() {
    return(
      <div className="whole-screen">
        {this.scoreboard()}
        <div className="centerpiece">{this.centerpiece()}</div>
        {this.endGameButton()}
      </div>
    );
  }
}

export default App;
