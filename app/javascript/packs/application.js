import React from "react";
import { render } from "react-dom";
import App from "../components/App";

document.addEventListener("DOMContentLoaded", () => {
  const initialGameStateNode = document.getElementById('initialGameState');
  const initialGameState = JSON.parse(initialGameStateNode.getAttribute('data-gamestate'));

  const initialHandNode = document.getElementById('initialHand');
  const initialHand = JSON.parse(initialHandNode.getAttribute('data-hand'));

  render(
    <App initialGameState={initialGameState} initialHand={initialHand}/>,
    document.body.appendChild(document.createElement("div"))
  );
});
