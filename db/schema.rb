# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_03_14_053030) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "game_moves", force: :cascade do |t|
    t.bigint "game_id", null: false
    t.bigint "game_player_id", null: false
    t.integer "move_number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "score", null: false
    t.index ["game_id"], name: "index_game_moves_on_game_id"
    t.index ["game_player_id"], name: "index_game_moves_on_game_player_id"
  end

  create_table "game_players", force: :cascade do |t|
    t.string "name", null: false
    t.bigint "game_id", null: false
    t.integer "order"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["game_id"], name: "index_game_players_on_game_id"
  end

  create_table "game_tiles", force: :cascade do |t|
    t.string "letter", null: false
    t.integer "points", null: false
    t.bigint "game_id", null: false
    t.bigint "in_hand_of_player_id"
    t.bigint "played_in_move_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["game_id"], name: "index_game_tiles_on_game_id"
    t.index ["in_hand_of_player_id"], name: "index_game_tiles_on_in_hand_of_player_id"
    t.index ["played_in_move_id"], name: "index_game_tiles_on_played_in_move_id"
  end

  create_table "games", force: :cascade do |t|
    t.boolean "has_started", default: false
    t.boolean "has_ended", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "game_moves", "game_players"
  add_foreign_key "game_moves", "games"
  add_foreign_key "game_players", "games"
  add_foreign_key "game_tiles", "game_moves", column: "played_in_move_id"
  add_foreign_key "game_tiles", "game_players", column: "in_hand_of_player_id"
  add_foreign_key "game_tiles", "games"
end
