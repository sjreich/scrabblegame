class AddScoreToMove < ActiveRecord::Migration[5.2]
  def change
    add_column :game_moves, :score, :integer
    GameMove.update_all(score: 0)
    change_column_null :game_moves, :score, false
  end
end
