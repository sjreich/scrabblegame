class CreatePlayers < ActiveRecord::Migration[5.2]
  def change
    create_table :games do |t|
      t.boolean :has_started, default: false
      t.boolean :has_ended, default: false
      t.timestamps
    end

    create_table :game_players do |t|
      t.string :name, null: false, unique: true
      t.belongs_to :game, foreign_key: true, null: false
      t.integer :order, unique: true
      t.timestamps
    end

    create_table :game_moves do |t|
      t.belongs_to :game, foreign_key: true, null: false
      t.belongs_to :game_player, foreign_key: true, null: false
      t.integer :move_number, unique: true
      t.timestamps
    end

    create_table :game_tiles do |t|
      t.string :letter, null: false
      t.integer :points, null: false
      t.belongs_to :game, foreign_key: true, null: false
      t.belongs_to :in_hand_of_player, foreign_key: { to_table: :game_players }
      t.belongs_to :played_in_move, foreign_key: { to_table: :game_moves }
      t.timestamps
    end
  end
end
